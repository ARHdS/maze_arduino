Maze Runner Project

Objective:

    .Create a maze runner robot that can solve a simple maze and optimize the route based on exploration/discovery.

Secondary objectives:

    .Learn Git
    .Recap on C++
    .Improve documentation technique
    .Start working on algorithms
    .Learn solving graph problems (maybe?)
    .Start a programmer porfolio

Project directives:

    .Adapt robot runner with Arduino Nano, distance sensors and bluetooth control
    .Use arduino code only (C++)
    .Record all progress in GitLab
    .KISS... all the way!
    .Mottos: "Code for Fun and Function" & "In pursuit of Flow"

Milestones:

    .Build wall maze
        _scout types of maze
        _choose maze
        _build maze
    .Optimize robot
        _change position of sensors
        _correct existing (non functional) programming for android control
        _prepare android app for maze solving mode ( discover run | solve run )
    .Program solver
        _available strategies for chosen maze
        _search and select adequate algorithm
    .Test solver and debug

Timeline:

    .8 weeks from start to finish making the final date 2021-08-31 (maybe too ambitious of a target?)
