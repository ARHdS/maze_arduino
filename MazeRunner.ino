// Maze Runner  - Version 0.2.3
// 
// Serial flushed @start (CONFIG_CONSTANTS()) and tighter (conditional) control of execution of modes.
// only using SoftwareSerial.h library.
// discovery() still not 100% tested.

#include <SoftwareSerial.h>

double ENA;
double IN1;
double IN2;
double ENB;
double IN3;
double IN4;
double TRIG_L;
double ECHO_LEFT;
double ECHO_FRONT;
double TRIG_F;
double ECHO_RIGHT;
double TRIG_R;
double LED;

double OFF;
double ON;
double speed1;
double speed2;
double COMANDO;
double DISTANCE_F;
double moving;
double min_dist_f;
double min_dist_l;
double min_dist_r;
double maze_drive;               // manual drive mode
double maze_discovery;          // maze discovery mode
double maze_run_optimal;        // run found path mode

float getDistance(int trig,int echo){
    pinMode(trig,OUTPUT);
    digitalWrite(trig,LOW);
    delayMicroseconds(2);
    digitalWrite(trig,HIGH);
    delayMicroseconds(10);
    digitalWrite(trig,LOW);
    pinMode(echo, INPUT);
    return pulseIn(echo,HIGH,30000)/58.0;
}

void CONFIG_CONECTORS();
void CONFIG_CONSTANTS();
void motorFRONT(double motor, double speed);
void motorREVERSE(double motor, double speed);
void motorSTOP(double motor);
void move_forward();
void stop_runner();
void move_backwards();
void rotate_left();
void rotate_right();
void discovery();
void run_optimal();

void CONFIG_CONECTORS()
{
    ENA = 10;
    IN1 = 9;
    IN2 = 8;
    ENB = 5;
    IN3 = 7;
    IN4 = 6;
    TRIG_L = A2;
    ECHO_LEFT = 11;
    ECHO_FRONT = 4;
    TRIG_F = A1;
    ECHO_RIGHT = 3;
    TRIG_R = A0;
    LED = 12;
}

void CONFIG_CONSTANTS()
{
    OFF = 0;
    ON = 1;
    speed1 = 126;
    speed2 = 92;
    maze_drive = 0;
    maze_discovery = 0;
    maze_run_optimal = 0;
    moving = 0;
    min_dist_f = 6;
    min_dist_l = 8;
    min_dist_r = 8;
    Serial.flush();
}

void motorFRONT(double motor, double speed)
{
    if(((motor)==(2))){
        analogWrite(ENA,speed);
        digitalWrite(IN1,OFF);
        digitalWrite(IN2,ON);
    }else{
        if(((motor)==(1))){
            analogWrite(ENB,speed);
            digitalWrite(IN3,OFF);
            digitalWrite(IN4,ON);
        }
    }
}

void motorREVERSE(double motor, double speed)
{
    if(((motor)==(2))){
        analogWrite(ENA,speed);
        digitalWrite(IN1,ON);
        digitalWrite(IN2,OFF);
    }else{
        if(((motor)==(1))){
            analogWrite(ENB,speed);
            digitalWrite(IN3,ON);
            digitalWrite(IN4,OFF);
        }
    }
}

void motorSTOP(double motor)
{
    if(((motor)==(2))){
        digitalWrite(IN1,ON);
        digitalWrite(IN2,ON);
    }else{
        if(((motor)==(1))){
            digitalWrite(IN3,ON);
            digitalWrite(IN4,ON);
        }
    }
}

void move_forward()
{
    motorFRONT(1,speed1);
    motorFRONT(2,speed2);
}

void stop_runner()
{
    motorSTOP(1);
    motorSTOP(2);
}

void move_backwards()
{
    motorREVERSE(1,speed1);
    motorREVERSE(2,speed2);
}

void rotate_left()
{
    motorFRONT(1,220);
    motorREVERSE(2,220);
    _delay(0.25);
    stop_runner();
    _delay(1);
}

void rotate_right()
{
    motorFRONT(2,220);
    motorREVERSE(1,220);
    _delay(0.25);
    stop_runner();
    _delay(1);
}

void discovery()
{
    DISTANCE_F = getDistance(11,2);
    if(((DISTANCE_F) < (min_dist_f))){
        stop_runner();
        _delay(1);
        rotate_right();
        moving = OFF;
    }
    if (((moving)==(OFF))){
        move_forward();
        moving = ON;
    }
}

void run_optimal()
{
    stop_runner();
}

void setup(){
    Serial.begin(9600);
    CONFIG_CONECTORS();
    CONFIG_CONSTANTS();
    pinMode(ENA,OUTPUT);
    pinMode(IN1,OUTPUT);
    pinMode(IN2,OUTPUT);
    pinMode(ENB,OUTPUT);
    pinMode(IN3,OUTPUT);
    pinMode(IN4,OUTPUT);
}

void loop(){
    if((Serial.available()) > (0)){
        COMANDO = Serial.read();
        Serial.println(COMANDO);
        if(((COMANDO)==(53))){
            maze_drive = ON ;
            maze_discovery = OFF ;
            maze_run_optimal = OFF ;
            COMANDO =  0 ;
        }
        if(((COMANDO)==(54))){
            maze_drive = OFF ;
            maze_discovery = ON ;
            maze_run_optimal = OFF ;
            moving = OFF ;
            COMANDO =  0 ;
        }
        if(((COMANDO)==(55))){
            maze_drive = OFF ;
            maze_discovery = OFF ;
            maze_run_optimal = ON ;
            COMANDO =  0 ;
        }
        if (((maze_drive)==(ON))){
            if(((COMANDO)==(48))){
            Serial.println("move_backwards");
            COMANDO =  0 ;
            move_backwards();
            }
            if(((COMANDO)==(49))){
                Serial.println("move_forward");
                COMANDO =  0 ;
                move_forward();
            }
            if(((COMANDO)==(50))){
                Serial.println("rotate_right");
                COMANDO =  0 ;
                rotate_right();
            }
            if(((COMANDO)==(51))){
                Serial.println("rotate_left");
                COMANDO =  0 ;
                rotate_left();
            }
            if(((COMANDO)==(52))){
                Serial.println("stop_runner");
                COMANDO =  0 ;
                stop_runner();
            }
        }
        if (((maze_discovery)==(ON))){
            discovery();
        }
        if (((maze_run_optimal)==(ON))){
            run_optimal();
        }
    }
    _loop();
}

void _delay(float seconds){
    long endTime = millis() + seconds * 1000;
    while(millis() < endTime)_loop();
}

void _loop(){
}
